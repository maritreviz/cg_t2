#include <GL/glut.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <vector>
using std::vector;
#include "extras.h"
#include <string.h>
#include "camera.h"

/// Estruturas iniciais para armazenar vertices
//  Você poderá utilizá-las adicionando novos métodos (de acesso por exemplo) ou usar suas próprias estruturas.
class vertice
{
    public:
        float x,y,z;
};

class triangle
{
    public:
        vertice v[3];
};

/// Globals
float zdist = 0.5;
float rotationX = 0.0, rotationY = 0.0;
float   last_x=0.0f, last_y=0.0f;
int width, height;
int grupo = 0;
float alturaZ = 0.30f;
float alturaY = 0.2f;
float cY = 0.0f;
vertice a, b,c,d, aux1, aux2, meuX;
vector< vector<vertice> > grupos1;     //Criação de lista de grupos, sendo cada grupo uma lista de vértices.
vector< vector<vertice> > grupos2;     //Criação de lista de grupos espelho,utilizada para a extrusão e criação do desenho 3D
vector<vertice> vet,vet2;             //Lista de vértices auxiliares utilizadas ao longo do código
vector<vertice> gaux;
char g, z;
char title[30] = "Grupo ";
vector< vector<vertice> > grupos3;
vector< vector<vertice> > grupos4;
//bool fullscreen=0;
FILE *arq;
float coordX,coordY;

bool edicao = false;

void Display();
void Reshape (int w, int h);
void Keyboard(unsigned char key, int x, int y);
void KeyboardUp(unsigned char key, int x, int y);
void MouseMotion(int x, int y);
void Mouse(int button, int state, int x, int y);
void Timer(int value);
void Idle();
void scene();
void setMaterial(void);
void init (void);
void Grid();

Camera g_camera;
bool 	g_key[256];
bool 	g_shift_down = false;
int 	g_viewport_width = 0;
int 	g_viewport_height = 0;
bool 	g_mouse_left_down = false;
bool	g_mouse_right_down = false;
bool	fullscreen = false;	// Fullscreen Flag Set To Fullscreen Mode By Default
bool 	inverseMouse = false;
bool	boostSpeed = false; // Change keyboard speed
bool  flyMode = false;
bool	releaseMouse = false;

// Movement settings
float g_translation_speed = 0.05;
float g_rotation_speed = M_PI/180*0.2;
float initialY = 2; // initial height of the camera (flymode off value)

void setMaterial(void)
{
	// Material do objeto (neste caso, ruby). Parametros em RGBA
	GLfloat objeto_ambient[]   = { .1745, .01175, .01175, 1.0 };
	GLfloat objeto_difusa[]    = { .91424, .04136, .04136, 1.0 };
	GLfloat objeto_especular[] = { .727811, .626959, .626959, 1.0 };
	GLfloat objeto_brilho[]    = { 90.0f };

	// Define os parametros da superficie a ser iluminada
	glMaterialfv(GL_FRONT, GL_AMBIENT, objeto_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, objeto_difusa);
	glMaterialfv(GL_FRONT, GL_SPECULAR, objeto_especular);
	glMaterialfv(GL_FRONT, GL_SHININESS, objeto_brilho);
}

void scene()
{ //desenho das paredes do entorno da cena
	float size = 110.0f;
	glDisable(GL_LIGHTING);
	glColor3f(0.9f, 0.9f, 0.9f);
	glPushMatrix();
		glScalef(size, .1, size);
		glutSolidCube(1);
	glPopMatrix();
	glColor3f(0.8f, 1.0f, 0.8f);
	glPushMatrix();
		glTranslatef(0.0f, 20.0f, 0.0f);
		glScalef(size, .2, size);
		glutSolidCube(1);
	glPopMatrix();

	glEnable(GL_LIGHTING);
	setMaterial();
	for(int j = -50; j <= 50; j+=10) //desenho dos pilares da cena
	{
		for(int i = -50; i <= 50; i+=10)
		{
			glPushMatrix();
			glTranslated(i, 10, j);
			glScalef(1.5, 20, 1.5);
			glutSolidCube(1);
			glPopMatrix();
		}
	}
	glDisable(GL_LIGHTING);
}
/// Functions
void init(void)
{
    float pos[3] = {0.0f, 2.0f, 5.0f};
	glClearColor (0.0, 0.0, 0.0, 0.0);
	glShadeModel (GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);               // Habilita Z-buffer
	glEnable(GL_LIGHTING);                 // Habilita luz
	glEnable(GL_LIGHT0);                   // habilita luz 0

	// Cor da fonte de luz (RGBA)
	GLfloat cor_luz[]         = { 1.0, 1.0, 1.0, 1.0};
	// Posicao da fonte de luz. Ultimo parametro define se a luz sera direcional (0.0) ou tera uma posicional (1.0)
	GLfloat posicao_luz[]     = { 50.0, 50.0, 50.0, 1.0};

	// Define parametros da luz
	glLightfv(GL_LIGHT0, GL_AMBIENT, cor_luz);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, cor_luz);
	glLightfv(GL_LIGHT0, GL_SPECULAR, cor_luz);
	glLightfv(GL_LIGHT0, GL_POSITION, posicao_luz);

	printf("NAVIGATOR - First Person Shooter (FPS) Example\n\n");
	printf("  - Press F11 to enable FullScreen\n");
	printf("  - Press 'w', 'a', 's', 'd' and mouse to move\n");
	printf("  - Press 'i' to inverse mouse (y direction)\n");
	printf("  - Press 'b' to increase/reduce speed\n");
	printf("  - Press 'f' to enable/disable Fly Mode\n");
	printf("  - Press 'r' to release/attach mouse cursor\n\n");

	g_camera.SetPos(pos[0], pos[1], pos[2]);

//setMaterials();
}

/* Exemplo de cálculo de vetor normal que são definidos a partir dos vértices do triângulo;
  v_2
  ^
  |\
  | \
  |  \       'vn' é o vetor normal resultante
  |   \
  +----> v_1
  v_0
*/
void CalculaNormal(triangle t, vertice *vn)
{
    vertice v_0 = t.v[0],
            v_1 = t.v[1],
            v_2 = t.v[2];
    vertice v1, v2;
    double len;

    /* Encontra vetor v1 */
    v1.x = v_1.x - v_0.x;
    v1.y = v_1.y - v_0.y;
    v1.z = v_1.z - v_0.z;

    /* Encontra vetor v2 */
    v2.x = v_2.x - v_0.x;
    v2.y = v_2.y - v_0.y;
    v2.z = v_2.z - v_0.z;

    /* Calculo do produto vetorial de v1 e v2 */
    vn->x = (v1.y * v2.z) - (v1.z * v2.y);
    vn->y = (v1.z * v2.x) - (v1.x * v2.z);
    vn->z = (v1.x * v2.y) - (v1.y * v2.x);

    /* normalizacao de n */
    len = sqrt(pow(vn->x,2) + pow(vn->y,2) + pow(vn->z,2));

    vn->x /= len;
    vn->y /= len;
    vn->z /= len;
}

void drawObject(vector< vector<vertice> > g1, vector< vector<vertice> > g2,vector< vector<vertice> > g3, vector< vector<vertice> > g4)
{

    vertice vetorNormal;
    double len;
    //cria os triangulos a partir dos dois vetores de vertices
    vector<triangle> tr;
    for(int i = 0; i < g1.size(); i ++)
    {
        if(g1.at(i).size()>1)
        {

            for(int j=0; j< g1.at(i).size()-1; j++)
            {
                triangle c,d,e,f,g,h,k,l,m,n,o,p;

                vertice v1, v2, vn, projecao;
                float alturaZ = g2.at(i).at(j+1).z;
                float distX =  g1.at(i).at(j+1).x - g1.at(i).at(j).x;
                float esp = g3.at(i).at(j+1).y -g1.at(i).at(j+1).y;

               // printf("Espessura %f\n", esp);

                v1.x = g1.at(i).at(j).x - g1.at(i).at(j+1).x;
                v1.y = g1.at(i).at(j).y - g1.at(i).at(j+1).y;
                v1.z = g1.at(i).at(j).z - g1.at(i).at(j+1).z;



                v2.x = g2.at(i).at(j+1).x - g1.at(i).at(j+1).x;
                v2.y = g2.at(i).at(j+1).y - g1.at(i).at(j+1).y;
                v2.z = g2.at(i).at(j+1).z - g1.at(i).at(j+1).z;


                vn.x = (v1.y * v2.z) - (v1.z * v2.y);
                vn.y = (v1.z * v2.x) - (v1.x * v2.z);
                vn.z = (v1.x * v2.y) - (v1.y * v2.x);

                /* normalizacao de n */
                len = sqrt(pow(vn.x,2) + pow(vn.y,2) + pow(vn.z,2));


                vn.x = -vn.x*esp/len*esp;
                vn.y = -vn.y*esp/len*esp;
                vn.z = -vn.z*esp/len*esp;


                projecao.x =  vn.x + g1.at(i).at(j+1).x;
                projecao.y = vn.y + g1.at(i).at(j+1).y;
                projecao.z = vn.z + g1.at(i).at(j+1).z;



                g1.at(i).at(j+1).x = projecao.x;
                g1.at(i).at(j+1).y = projecao.y;
                g1.at(i).at(j+1).z = projecao.z;


                g2.at(i).at(j+1).x = projecao.x;
                g2.at(i).at(j+1).y = projecao.y;

                projecao.x =  -vn.x + g1.at(i).at(j+1).x;
                projecao.y = -vn.y + g1.at(i).at(j+1).y;
                projecao.z = -vn.z + g1.at(i).at(j+1).z;

                g3.at(i).at(j+1).x = projecao.x;
                g3.at(i).at(j+1).y = projecao.y;
                g3.at(i).at(j+1).z = projecao.z;

                g4.at(i).at(j+1).x = projecao.x;
                g4.at(i).at(j+1).y = projecao.y;


                v1.x = g1.at(i).at(j).x - g1.at(i).at(j+1).x;
                v1.y = g1.at(i).at(j).y - g1.at(i).at(j+1).y;
                v1.z = g1.at(i).at(j).z - g1.at(i).at(j+1).z;

                v2.x = vn.x;
                v2.y = vn.y;
                v2.z = vn.z;

                vn.x = (v1.y * v2.z) - (v1.z * v2.y);
                vn.y = (v1.z * v2.x) - (v1.x * v2.z);
                vn.z = (v1.x * v2.y) - (v1.y * v2.x);

                /* normalizacao de n */
                len = sqrt(pow(vn.x,2) + pow(vn.y,2) + pow(vn.z,2));


                vn.x = -vn.x*alturaZ/len*alturaZ;
                vn.y = -vn.y*alturaZ/len*alturaZ;
                vn.z = -vn.z*alturaZ/len*alturaZ;



                projecao.z = -vn.z + g1.at(i).at(j+1).z;

                g4.at(i).at(j+1).z = projecao.z;
                g2.at(i).at(j+1).z = projecao.z;

                if(j==0)
                {

                    v1.x = g1.at(i).at(j+1).x - g1.at(i).at(j).x;
                    v1.y = g1.at(i).at(j+1).y - g1.at(i).at(j).y;
                    v1.z = g1.at(i).at(j+1).z - g1.at(i).at(j).z;

                    len = sqrt(pow(vn.x,2) + pow(vn.y,2) + pow(vn.z,2));

                    distX = g1.at(i).at(j+1).x - g1.at(i).at(j).x;



                    vn.x = -v1.x*distX/len*distX;
                    vn.y =- v1.y*distX/len*distX;
                    vn.z = -v1.z*distX/len*distX;



                    g1.at(i).at(j).x = vn.x + g1.at(i).at(j+1).x;
                    g1.at(i).at(j).y = vn.y + g1.at(i).at(j+1).y;
                    g1.at(i).at(j).z = vn.z + g1.at(i).at(j+1).z;

                    g2.at(i).at(j).x = vn.x + g2.at(i).at(j+1).x;
                    g2.at(i).at(j).y = vn.y + g2.at(i).at(j+1).y;
                    g2.at(i).at(j).z = vn.z + g2.at(i).at(j+1).z;

                    g3.at(i).at(j).x = vn.x + g3.at(i).at(j+1).x;
                    g3.at(i).at(j).y = vn.y + g3.at(i).at(j+1).y;
                    g3.at(i).at(j).z = vn.z + g3.at(i).at(j+1).z;

                    g4.at(i).at(j).x = vn.x + g4.at(i).at(j+1).x;
                    g4.at(i).at(j).y = vn.y + g4.at(i).at(j+1).y;
                    g4.at(i).at(j).z = vn.z + g4.at(i).at(j+1).z;


                }


                c = {g3.at(i).at(j), g1.at(i).at(j), g3.at(i).at(j+1)};
                d = {g1.at(i).at(j), g1.at(i).at(j+1), g3.at(i).at(j+1)};
                e = {g4.at(i).at(j), g4.at(i).at(j+1), g2.at(i).at(j)};
                f = {g2.at(i).at(j), g4.at(i).at(j+1), g2.at(i).at(j+1)};

                g = {g3.at(i).at(j), g4.at(i).at(j), g2.at(i).at(j)};
                h = {g3.at(i).at(j), g2.at(i).at(j), g1.at(i).at(j)};
                k = {g3.at(i).at(j+1), g2.at(i).at(j+1), g4.at(i).at(j+1)};
                l = {g3.at(i).at(j+1), g1.at(i).at(j+1), g2.at(i).at(j+1)};

                m = {g3.at(i).at(j), g3.at(i).at(j+1), g4.at(i).at(j+1)};
                n = {g4.at(i).at(j+1), g4.at(i).at(j), g3.at(i).at(j)};
                o = {g1.at(i).at(j), g2.at(i).at(j+1), g1.at(i).at(j+1)};
                p = {g1.at(i).at(j), g2.at(i).at(j), g2.at(i).at(j+1)};

                tr.push_back(c);
                tr.push_back(d);
                tr.push_back(e);
                tr.push_back(f);
                tr.push_back(g);
                tr.push_back(h);
                tr.push_back(k);
                tr.push_back(l);
                tr.push_back(m);
                tr.push_back(n);
                tr.push_back(o);
                tr.push_back(p);
                if(j==g1.at(i).size()-2)
                {
                        glBegin(GL_TRIANGLES);
                        for(int i = 0; i < tr.size(); i++) // triangulos
                        {
                            CalculaNormal(tr.at(i), &vetorNormal); // Passa face triangular e endereço do vetor normal de saída
                            glNormal3f(vetorNormal.x, vetorNormal.y,vetorNormal.z);
                            for(int j = 0; j < 3; j++)
                            { // vertices do triangulo
                                glVertex3d(tr.at(i).v[j].x, tr.at(i).v[j].y, tr.at(i).v[j].z);
                            }
                        }
                        glEnd();
                }
            }

        }
    }
}

// Eixos coordenados
void DesenhaEixos()
{
    glBegin(GL_LINES);
        glVertex3f(-1.0, 0.0, 0.0);
        glVertex3f(1.0, 0.0, 0.0);
        glVertex3f(0.0, -1.0, 0.0);
        glVertex3f(0.0, 1.0, 0.0);
    glEnd();
}

//pontinhos
void DesenhaPonto(int x, int y)
{
    glPointSize(3);
    glBegin(GL_POINTS);
        glVertex3f(x, y, 0.0);
    glEnd();
}
void desenha3D(){
    drawObject(grupos1,grupos2,grupos3,grupos4);
}
void escreveArquivo(char arquivo[20]) {
	char str1[10] = ".txt";
	strcat(arquivo, str1);
	arq = fopen(arquivo, "w"); //abre o arquivo txt
	//fprintf(arq, "Grupo X Y alturaZ\n");
	printf("Salvando modelo");
	//escrevendo no arquivo
	for (int i = 0; i < grupos1.size(); i++)
	{
		for (int j = 0; j < grupos1.at(i).size(); j++)
		{

			fprintf(arq, "%d %f %f %f %f\n", i, grupos1.at(i).at(j).x, grupos1.at(i).at(j).y, grupos2.at(i).at(j).z, (grupos3.at(i).at(j).y - grupos1.at(i).at(j).y));

		}
	}
	fclose(arq);

}
void leArquivo(char arquivo[20]) {
	char str[10] = ".txt";
	strcat(arquivo, str);
	arq = fopen(arquivo, "r"); //abre o arquivo txt
	printf("Lendo arquivo");
	//enquanto não for fim de arquivo o looping será executado
	 //e será impresso o texto
	while ((fscanf(arq, "%d %f %f %f %f\n", &grupo, &coordX, &coordY, &alturaZ, &alturaY)) != EOF) {
		a = { (float)coordX,(float)coordY,0.0 };
		b = { (float)coordX,(float)coordY,alturaZ };
		c = { (float)coordX,(float)coordY + alturaY,0.0 };
		d = { (float)coordX,(float)coordY + alturaY,alturaZ };
		if (grupo + 1 > grupos1.size())  // Caso o teclado selecione um grupo que ainda nao tenha sido criado, por ter ID menor que o  tamanho da lista de grupos
		{
			vector<vertice> gaux;  //cria uma lista de vertices auxiliar
			grupos1.push_back(gaux); //Insere na lista de grupos
			grupos2.push_back(gaux);  //Insere na lista de grupos auxiliar utilizada para extrusao
			grupos3.push_back(gaux);
			grupos4.push_back(gaux);
			printf("grupo criado na leitura %d\n", grupo);
		}
		grupos1.at(grupo).push_back(a); //adiciona a à lista de vertices com altura de z=0 em grupos 1
		grupos2.at(grupo).push_back(b); //adiciona b à lista de vértices com a altura selecionada de z em grupos2 que é parte do espelho de grupos1, uma lista auxiliar criada para facilitar a extrusão
		grupos3.at(grupo).push_back(c);
		grupos4.at(grupo).push_back(d);
		printf("%f %f %f %f\n", coordX, coordY, alturaZ, alturaY);
	}

	desenha3D();
	printf("Arquivo foi lido");
}
void showMenu()
{
    int op;
    system("cls");
    printf("\n=== MENU ===");
    printf("\ns - Salva o arquivo");
    printf("\nl - Carrega arquivo salvo na pasta");
    printf("\nF12 - Visualização em Modo Tela Cheia");
    printf("\n . - Aumentar a Espessura do Modelo");
    printf("\n , - Diminuir a Espessura do Modelo\n");
    printf("\n Setas direcionais para esquerda e Direita - Alternam grupos de desenho \n") ;
    printf("\n Setas direcionais para cima e baixo - Aumenta e Diminui altura do modelo \n") ;
    printf("\nEsc - Sair\n");

}
void display(void)
{
    if(edicao){
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(1,1,1,0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();


    glViewport(0,0,width/2,height); //VIEWPORT DO EIXO DE COORDENADAS
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-1,1,-1,1,-1,1);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();


    DesenhaEixos();
    if(grupos1.size()!=0)
    {
        for(int i=0; i<grupos1.size(); i++)
        {
            if(grupos1.at(i).size()!=0)
            {
                for(int j =0; j < grupos1.at(i).size();j++)
                {

                            glPushMatrix();
                            glColor3f(0, 1,0);
                            glTranslatef(grupos1.at(i).at(j).x, grupos1.at(i).at(j).y, 0);
                            DesenhaPonto(0, 0);
                            glPopMatrix();
                }
            }

        }
    }

    glViewport(width/2,0,width/2,height);     //VIEW PORT DO MODELO 3D

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    gluPerspective(60.0, (GLfloat) width/(GLfloat) height, 0.1, 200.0);


    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();


    gluLookAt (0.0, 0.0, zdist+0.5, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
    glPushMatrix();
        glRotatef( rotationY, 0.0, 0.3, 0.0 );
        glRotatef( rotationX, 0.3, 0.0, 0.0 );
        if(grupos1.size()!=0) // Se a lista de grupos nao é vazia
            {
                    desenha3D();
            }
    glPopMatrix();
    glutSwapBuffers();
    } else{
	glClearColor (0.0,0.0,0.0,1.0); //clear the screen to black
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //clear the color buffer and the depth buffer
	glViewport (0, 0, (GLsizei)g_viewport_width, (GLsizei)g_viewport_height); //set the viewport to the current window specifications
	glMatrixMode (GL_PROJECTION); //set the matrix to projection

	glLoadIdentity ();
	gluPerspective (60, (GLfloat)g_viewport_width / (GLfloat)g_viewport_height, 0.1 , 1000.0); //set the perspective (angle of sight, width, height, ,depth)
	glMatrixMode (GL_MODELVIEW); //set the matrix back to model
	g_camera.Refresh();
	glColor3f(0,1,0);

	// Draw a scene
	scene();
	glutSwapBuffers(); //swap the buffers
	}

}
void KeyboardUp(unsigned char key, int x, int y)
{
	g_key[key] = false;
}

void Timer(int value)
{
	float speed = g_translation_speed;

	if(g_key['w'] || g_key['W'])
	{
		g_camera.Move(speed, flyMode);
	}
	else if(g_key['s'] || g_key['S'])
	{
		g_camera.Move(-speed, flyMode);
	}
	else if(g_key['a'] || g_key['A'])
	{
		g_camera.Strafe(speed);
	}
	else if(g_key['d'] || g_key['D'])
	{
		g_camera.Strafe(-speed);
	}

	glutTimerFunc(1, Timer, 0);
}
void idle ()
{
  display();
}

void reshape (int w, int h)
{
   // if(edicao){
    width = w;
    height = h;
    g_viewport_width = w;
	g_viewport_height = h;

    glViewport (0, 0, (GLsizei) w, (GLsizei) h);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    if(edicao)
    gluPerspective(60.0, (GLfloat) w/(GLfloat) h, 0.01, 200.0);
    else
    gluPerspective (60, (GLfloat)w / (GLfloat)h, 0.1 , 1000.0); //set the perspective (angle of sight, width, height, ,depth)

    glMatrixMode (GL_MODELVIEW);
   /* } else {
        g_viewport_width = w;
	g_viewport_height = h;

	glViewport (0, 0, (GLsizei)w, (GLsizei)h); //set the viewport to the current window specifications
	glMatrixMode (GL_PROJECTION); //set the matrix to projection

	glLoadIdentity ();
	gluPerspective (60, (GLfloat)w / (GLfloat)h, 0.1 , 1000.0); //set the perspective (angle of sight, width, height, ,depth)
	glMatrixMode (GL_MODELVIEW); //set the matrix back to model
    }*/
}

// Special Keys callback
void specialKeysPress(int key, int x, int y)
{
    switch(key)
    {
        case GLUT_KEY_RIGHT:
            grupo += 1; //Passa para o próximo grupo na lista de seleção pelo teclado
            g = grupo + '0';
            sprintf(title, "Grupo: %d - Altura Z: %f - Espessura: %f", grupo, alturaZ, alturaY);
            glutSetWindowTitle(title);
            if(grupo +1 > grupos1.size() )  // Caso o teclado selecione um grupo que ainda nao tenha sido criado, por ter ID menor que o  tamanho da lista de grupos
            {    vector<vertice> gaux;  //cria uma lista de vertices auxiliar
                grupos1.push_back(gaux); //Insere na lista de grupos
                grupos2.push_back(gaux);  //Insere na lista de grupos auxiliar utilizada para extrusao
                grupos3.push_back(gaux);
                grupos4.push_back(gaux);
                printf("grupo:criado %d\n", grupo);
            }
            break;
        case GLUT_KEY_LEFT:
            if(grupo > 0){  //Verifica se é valido percorrer a lista de grupos para a esquerda
                grupo -= 1;  // Passa para o grupo anterior na lista de seleção pelo teclado
                g = grupo + '0';
            sprintf(title, "Grupo: %d - Altura Z: %f - Espessura: %f", grupo, alturaZ, alturaY); //muda o título da tela
            glutSetWindowTitle(title);
            }
            break;
        case GLUT_KEY_UP:
            alturaZ += 0.1;
            sprintf(title, "Grupo: %d - Altura Z: %f - Espessura: %f", grupo, alturaZ, alturaY);
            glutSetWindowTitle(title);
            break;
        case GLUT_KEY_DOWN:
            alturaZ -= 0.1;
            sprintf(title, "Grupo: %d - Altura Z: %f - Espessura: %f", grupo, alturaZ, alturaY);
            glutSetWindowTitle(title);
            break;
        case GLUT_KEY_F12:
                if (fullscreen)
                {
                    glutReshapeWindow(800, 400);
                    fullscreen =0;
                } else {
                    glutFullScreen();
                    fullscreen =1;
                }
            break;
        default:
            printf("\nPressionou outra tecla especial não mapeada!");
            break;
    }
    glutPostRedisplay();
}

void keyboard(unsigned char key, int x, int y)
{

	char nome[20];
	switch (tolower(key))
	{
	case 27:
		exit(0);
		break;
	case 's':
		printf("Digite o nome do arquivo: ");
		scanf("%s", &nome);
		escreveArquivo(nome);
		break;
	case 'l':
		printf("Digite o nome do arquivo: ");
		scanf("%s", &nome);
		leArquivo(nome);
		break;
	case ',':
		alturaY -= 0.01f;
		sprintf(title, "Grupo: %d - Altura Z: %f - Espessura: %f", grupo, alturaZ, alturaY);
        glutSetWindowTitle(title);
		break;
	case '.':
		alturaY += 0.01f;
		sprintf(title, "Grupo: %d - Altura Z: %f - Espessura: %f", grupo, alturaZ, alturaY);
        glutSetWindowTitle(title);
		break;
		case 'i':
			inverseMouse = !inverseMouse;
			inverseMouse ? printf("InverseMouse ON\n") : printf("InverseMouse OFF\n");
		break;
		case 'b':
			boostSpeed = !boostSpeed;
			(boostSpeed) ? g_translation_speed = 0.2 : g_translation_speed = 0.05;
			boostSpeed ? printf("BoostMode ON\n") : printf("BoostMode OFF\n");
		break;
		case 'f':
			flyMode = !flyMode;
			if(flyMode)
			{
				printf("FlyMode ON\n");
			}
			else
			{
				float x, y, z;
				printf("FlyMode OFF\n");
				g_camera.GetPos(x, y, z);
				g_camera.SetPos(x, initialY, z);
			}

		break;
		case 'r':
			releaseMouse = !releaseMouse;
			releaseMouse ? printf("Mouse released\n") : printf("Mouse Attached\n");
		break;
    case 'm':
            if(edicao){
                glutSetCursor(GLUT_CURSOR_NONE);
               // releaseMouse = false;
              // inverseMouse = false;
                edicao = false;
                printf("\n modo navegaçaõ");
            }
            else{
                glutSetCursor(GLUT_CURSOR_INHERIT);
               // releaseMouse = true;
               //inverseMouse = true;
                edicao = true;
            printf("\n modo edicao");
            }
            break;

	}
}

// Motion callback
void motion(int x, int y)
{
	if(edicao){
        if (x > g_viewport_width / 2 + 1) {
		rotationX += (float)(y - last_y);
		rotationY += (float)(x - last_x);
	}
	last_x = x;
	last_y = y;
	} else{
	// This variable is hack to stop glutWarpPointer from triggering an event callback to Mouse(...)
	// This avoids it being called recursively and hanging up the event loop
	static bool just_warped = false;

	if(just_warped)
	{
		just_warped = false;
		return;
	}

	int dx = x - g_viewport_width/2;
	int dy = y - g_viewport_height/2;

	if(inverseMouse) dy = g_viewport_height/2-y;

	if(dx) g_camera.RotateYaw(g_rotation_speed*dx);
	if(dy) g_camera.RotatePitch(g_rotation_speed*dy);

	if(!releaseMouse)	glutWarpPointer(g_viewport_width/2, g_viewport_height/2);

	just_warped = true;
	}
}

// Mouse callback
void mouse(int button, int state, int x, int y)
{
    if(edicao){
    if ( button == GLUT_LEFT_BUTTON)
    {
        if(state == GLUT_DOWN && x < width/2+1)
        {
            printf("\nBotao esquerdo pressionado na posicao [%d, %d].", x, y);
            last_x = float(x)/(width/4)-1;
            last_y = 1-float(y)/(height/2);
            a = {(float)last_x,(float)last_y,0.0};
            b = {(float)last_x,(float)last_y,alturaZ};
            c = {(float)last_x,last_y+alturaY,0.0};
            d = {(float)last_x,last_y+alturaY,alturaZ};
            grupos1.at(grupo).push_back(a); //adiciona a à lista de vertices com altura de z=0 em grupos 1
            grupos2.at(grupo).push_back(b); //adiciona b à lista de vértices com a altura selecionada de z em grupos2 que é parte do espelho de grupos1, uma lista auxiliar criada para facilitar a extrusão
            grupos3.at(grupo).push_back(c);
            grupos4.at(grupo).push_back(d);
            printf("\nlast_x e last_y receberão [%f, %f]. altura: %f", last_x, last_y, alturaZ);
            for(int i=0; i<grupos2.at(grupo).size(); i++)
            {
                printf("\nVetor de Vertices em : [%f, %f, %f].",  grupos2.at(grupo).at(i).x, grupos2.at(grupo).at(i).y, grupos2.at(grupo).at(i).z );
            }

        }
        else
            printf("\nBotao esquerdo solto na posicao [%d, %d].", x, y);  // GLUT_UP
    }
    if ( button == GLUT_RIGHT_BUTTON)
    {
        if(state == GLUT_DOWN)
        {
            if(grupos1.at(grupo).size()!=0)
            {
                grupos1.at(grupo).pop_back();      //Remoção do último vértice inserido ao clique do botao direito do mouse
                grupos2.at(grupo).pop_back();
                grupos3.at(grupo).pop_back();
                grupos4.at(grupo).pop_back();
            } else {
            printf("\n Todos os vértices do grupo foram apagados\n");

            }
        }
    }
    if(button == 3) // Scroll up
    {
        zdist+=1.0f;
    }
    if(button == 4) // Scroll Down
    {
        zdist-=1.0f;
    }
    }
}

/// Main
int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    showMenu();
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize (800, 400);
    glutInitWindowPosition (300, 200);
    glutCreateWindow (argv[0]);
    grupos1.push_back(gaux);
    grupos2.push_back(gaux);
    grupos3.push_back(gaux);
    grupos4.push_back(gaux);
    printf("grupo:criado %d\n", grupo);
    sprintf(title, "Grupo: %d - Altura Z: %f - Espessura: %f", grupo, alturaZ, alturaY);
    glutSetWindowTitle(title);

    glutIgnoreKeyRepeat(1);

    init ();
    glutDisplayFunc(display);
	glutIdleFunc(display);
    glutReshapeFunc(reshape);
    glutMouseFunc( mouse );
    glutMotionFunc( motion );
	glutPassiveMotionFunc(motion);
    glutSpecialFunc( specialKeysPress );
	glutKeyboardUpFunc(KeyboardUp);
    glutKeyboardFunc(keyboard);
    glutIdleFunc(idle);
	glutTimerFunc(1, Timer, 0);
    glutMainLoop();
    return 0;
}
