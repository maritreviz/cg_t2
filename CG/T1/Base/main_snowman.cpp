#include <GL/glut.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <vector>
using std::vector;
#include "extras.h"
#include <string.h>

/// Estruturas iniciais para armazenar vertices
//  Você poderá utilizá-las adicionando novos métodos (de acesso por exemplo) ou usar suas próprias estruturas.
class vertice
{
    public:
        float x,y,z;
};

class triangle
{
    public:
        vertice v[3];
};

class ponto
{
    public:
        int x,y,z;
};

class snowman
{
    public:
        int x,y,z,w;
};

/// Globals
float zdist = 0.5;
float rotationX = 0.0, rotationY = 0.0;
float   last_x=0.0f, last_y=0.0f;
int width, height;
int grupo = 0;
float alturaZ = 0.30f;
float esp = 0.2f;
float cY = 0.0f;
vertice a, b,c,d, aux1, aux2, meuX;
vector<vertice> vet,vet2;             //Lista de vértices auxiliares utilizadas ao longo do código
vector<vertice> gaux;
char g, z;
char title[30] = "Grupo ";
bool fullscreen=0;
FILE *arq;
float coordX,coordY;
vertice v0 = {0,0,0};
vector<vertice> vertices;

vector<ponto> triangulos;
vector<triangle> triangulosPLY;
vector<vertice> normais;
vector<snowman> id_triangulos;
float zom = 0.1;
int qtd_triangulos;

void leArquivoPLYCow(char arquivo[20]);

/// Functions
void init(void)
{
    initLight(width, height); // Função extra para tratar iluminação.
    setMaterials();
}

/* Exemplo de cálculo de vetor normal que são definidos a partir dos vértices do triângulo;
  v_2
  ^
  |\
  | \
  |  \       'vn' é o vetor normal resultante
  |   \
  +----> v_1
  v_0
*/
void CalculaNormal(triangle t, vertice *vn)
{
    vertice v_0 = t.v[0],
            v_1 = t.v[1],
            v_2 = t.v[2];
    vertice v1, v2;
    double len;

    /* Encontra vetor v1 */
    v1.x = v_1.x - v_0.x;
    v1.y = v_1.y - v_0.y;
    v1.z = v_1.z - v_0.z;

    /* Encontra vetor v2 */
    v2.x = v_2.x - v_0.x;
    v2.y = v_2.y - v_0.y;
    v2.z = v_2.z - v_0.z;

    /* Calculo do produto vetorial de v1 e v2 */
    vn->x = (v1.y * v2.z) - (v1.z * v2.y);
    vn->y = (v1.z * v2.x) - (v1.x * v2.z);
    vn->z = (v1.x * v2.y) - (v1.y * v2.x);

    /* normalizacao de n */
    len = sqrt(pow(vn->x,2) + pow(vn->y,2) + pow(vn->z,2));

    vn->x /= len;
    vn->y /= len;
    vn->z /= len;
}

void constroiTriangulos(){
    triangle t;
    vertice v1,v2,v3;
    for(int i = 0; i < triangulos.size(); i++){
        v1 = {vertices.at(triangulos.at(i).x).x, vertices.at(triangulos.at(i).x).y, vertices.at(triangulos.at(i).x).z};
        v2 = {vertices.at(triangulos.at(i).y).x, vertices.at(triangulos.at(i).y).y, vertices.at(triangulos.at(i).y).z};
        v3 = {vertices.at(triangulos.at(i).z).x, vertices.at(triangulos.at(i).z).y, vertices.at(triangulos.at(i).z).z};
        t = {v1,v2,v3};
        triangulosPLY.push_back(t);
    }
}
void constroiTriangulosSnowman(){
    triangle t;
    vertice v1,v2,v3,v4;
    //printf("\n \ntriangulos sendo construidos %d", id_triangulos.size());
    for(int i = 0; i < id_triangulos.size(); i++){
        if(id_triangulos.at(i).w == -1){
            v1 = {vertices.at(id_triangulos.at(i).x).x, vertices.at(id_triangulos.at(i).x).y, vertices.at(id_triangulos.at(i).x).z};
            v2 = {vertices.at(id_triangulos.at(i).y).x, vertices.at(id_triangulos.at(i).y).y, vertices.at(id_triangulos.at(i).y).z};
            v3 = {vertices.at(id_triangulos.at(i).z).x, vertices.at(id_triangulos.at(i).z).y, vertices.at(id_triangulos.at(i).z).z};
            t = {v1,v2,v3};
            triangulosPLY.push_back(t);
        } else {
            v1 = {vertices.at(id_triangulos.at(i).x).x, vertices.at(id_triangulos.at(i).x).y, vertices.at(id_triangulos.at(i).x).z};
            v2 = {vertices.at(id_triangulos.at(i).y).x, vertices.at(id_triangulos.at(i).y).y, vertices.at(id_triangulos.at(i).y).z};
            v3 = {vertices.at(id_triangulos.at(i).z).x, vertices.at(id_triangulos.at(i).z).y, vertices.at(id_triangulos.at(i).z).z};
            v4 = {vertices.at(id_triangulos.at(i).w).x, vertices.at(id_triangulos.at(i).w).y, vertices.at(id_triangulos.at(i).w).z};
            t = {v1,v2,v3};
            triangulosPLY.push_back(t);
            t = {v4,v2,v3};
            triangulosPLY.push_back(t);
        }
    }
    printf("\n \ntriangulos construidos");
}

void desenhaPLY(){
    vertice vetorNormal;
    qtd_triangulos = triangulosPLY.size();
    //printf("\n teste desenho PLY: %f", vertices.at(triangulos.at(0).v[0].x).y);
    glBegin(GL_TRIANGLES);
        for(int i = 0; i < triangulosPLY.size(); i++) // triangulos
        {
            CalculaNormal(triangulosPLY.at(i), &vetorNormal); // Passa face triangular e endereço do vetor normal de saída
            glNormal3f(vetorNormal.x, vetorNormal.y,vetorNormal.z);
            for(int j = 0; j < 3; j++) // vertices do triangulo
                glVertex3d(triangulosPLY.at(i).v[j].x, triangulosPLY.at(i).v[j].y, triangulosPLY.at(i).v[j].z);
        }
    glEnd();
   // printf("\ndesenho finalizado");
}

void leArquivoPLY(char arquivo[20]) {
    int flag = 0;
    int cont = 0;
    float cx,cy,cz,descarte;
    int tres,t1,t2,t3;
    ponto t;
    vertice v;
    char delim[] = " ";
    char line[128];
    float cordx,cordy,cordz;
	char str[10] = ".ply";
	strcat(arquivo, str);
	arq = fopen(arquivo, "r"); //abre o arquivo txt
	printf("Lendo arquivo");
	//enquanto não for fim de arquivo o looping será executado
	 //e será impresso o texto
    if(arq != NULL){
        while(fgets(line, sizeof line, arq) != NULL){
           // printf("%s  contador %d",line,cont);
            cont++;
            if(cont >= 10)
                flag = 1;
            if(flag == 1){
               // printf("%s flag: %d",line,flag);
                while ((fscanf(arq, "%f %f %f\n", &cx, &cy, &cz, &descarte)) != EOF && cx != 3) {
                    v.x = cx;
                    v.y = cy;
                    v.z = cz;
                   // printf("vertices: %f %f %f \n", cx, cy, cz);
                    vertices.push_back(v);
                }
                 //   printf("v.x %f %f %f ", vertices.at(0).x, vertices.at(0).y, vertices.at(0).z);
               // printf("TT triangulos: %d %d %d", (int)cx, (int)cy, (int)cz);
                t1 = (int)cx;
                t2= (int)cy;
                fscanf(arq, "%d",&t3);
                t.x = t1;
                t.y = t2;
                t.z = t3;
               // printf(" t: %d", t3);

                while ((fscanf(arq, "%d %d %d %d\n", &tres, &t1, &t2, &t3)) != EOF) {
                    t.x = t1;
                    t.y = t2;
                    t.z = t3;
                   // printf("triangulos: %d %d %d %d\n", tres, t1, t2, t3);
                    triangulos.push_back(t);
                }
               // printf("triangulos inseridos.. ");
            }
        }
        fclose(arq);
        printf("Arquivo foi lido");
        constroiTriangulos();
    }

}
void leArquivoPLYCow(char arquivo[20]) {
    int flag = 0;
    int cont = 0;
    float cx,cy,cz,descarte;
    int tres,t1,t2,t3;
    ponto t;
    vertice v;
    char delim[] = " ";
    char line[128];
    float cordx,cordy,cordz;
	char str[10] = ".ply";
	strcat(arquivo, str);
	arq = fopen(arquivo, "r"); //abre o arquivo txt
	printf("Lendo arquivo");
	//enquanto não for fim de arquivo o looping será executado
	 //e será impresso o texto
    if(arq != NULL){
        while(fgets(line, sizeof line, arq) != NULL){
           // printf("%s  contador %d",line,cont);
            cont++;
            if(cont >= 9)
                flag = 1;
            if(flag == 1){
                printf("%s flag: %d",line,flag);
                while ((fscanf(arq, "%f %f %f\n", &cx, &cy, &cz, &descarte)) != EOF && cx != 3) {
                    v.x = cx;
                    v.y = cy;
                    v.z = cz;
                   // printf("vertices: %f %f %f \n", cx, cy, cz);
                    vertices.push_back(v);
                }
                 //   printf("v.x %f %f %f ", vertices.at(0).x, vertices.at(0).y, vertices.at(0).z);
               // printf("TT triangulos: %d %d %d", (int)cx, (int)cy, (int)cz);
                t1 = (int)cx;
                t2= (int)cy;
                fscanf(arq, "%d",&t3);
                t.x = t1;
                t.y = t2;
                t.z = t3;
               // printf(" t: %d", t3);

                while ((fscanf(arq, "%d %d %d %d\n", &tres, &t1, &t2, &t3)) != EOF) {
                    t.x = t1;
                    t.y = t2;
                    t.z = t3;
                   // printf("triangulos: %d %d %d %d\n", tres, t1, t2, t3);
                    triangulos.push_back(t);
                }
               // printf("triangulos inseridos.. ");
            }
        }
        fclose(arq);
        printf("Arquivo foi lido");
        constroiTriangulos();
    }

}
void leArquivoPLYSnowman(char arquivo[20]) {
    int flag = 0;
    int cont = 0;
    float cx,cy,cz,descarte,nx,ny,nz;
    snowman s;
    int tres,t1,t2,t3,t4;
    ponto t;
    vertice v,n;
    char delim[] = " ";
    char line[128];
    float cordx,cordy,cordz;
	char str[10] = ".ply";
	strcat(arquivo, str);
	arq = fopen(arquivo, "r"); //abre o arquivo txt
	printf("Lendo arquivo");
	//enquanto não for fim de arquivo o looping será executado
	 //e será impresso o texto
    if(arq != NULL){
        while(fgets(line, sizeof line, arq) != NULL){
          //  printf("%s  contador %d",line,cont);
            cont++;
            if(cont >= 14)
                flag = 1;
            if(flag == 1){
               // printf("%s flag: %d",line,flag);
                while ((fscanf(arq, "%f %f %f %f %f %f\n", &cx, &cy, &cz, &nx, &ny, &nz)) != EOF && cx != 4) {
                    v.x = cx;
                    v.y = cy;
                    v.z = cz;
                    n.x = nx;
                    n.y = ny;
                    n.z = nz;
                   // printf("vertices: %f %f %f \n", cx, cy, cz);
                    vertices.push_back(v);
                    normais.push_back(n);
                }
                 //   printf("v.x %f %f %f ", vertices.at(0).x, vertices.at(0).y, vertices.at(0).z);
               // printf("TT triangulos: %d %d %d", (int)cx, (int)cy, (int)cz);
                t1 = (int)cx;
                t2 = (int)cy;
                fscanf(arq, "%d",&t3);
                t.x = t1;
                t.y = t2;
                t.z = t3;
               // printf(" t: %d", t3);

                while ((fscanf(arq, "%d", &tres) != EOF)) {
                    if(tres == 4){
                        fscanf(arq, "%d %d %d %d", &t1, &t2, &t3, &t4);
                        s.x = t1;
                        s.y = t2;
                        s.z = t3;
                        s.w = t4;
                       // printf("\n poligonos: %d %d %d %d %d\n", tres, t1, t2, t3, t4);
                        id_triangulos.push_back(s);
                    }
                    else if(tres == 3){
                        fscanf(arq, "%d %d %d", &t1, &t2, &t3);
                        s.x = t1;
                        s.y = t2;
                        s.z = t3;
                        s.w = -1;
                        id_triangulos.push_back(s);
                       // printf("\n triangulos: %d %d %d\n", tres, t1, t2, t3);
                    }
                }
               // printf("triangulos inseridos.. ");
            }
        }
        fclose(arq);
        printf("Arquivo foi lido");
        constroiTriangulosSnowman();
    }

}

void leArquivoPLYBunny(char arquivo[20]) {
    int flag = 0;
    int cont = 0;
    int cont2 =0;
    float cx,cy,cz,confidence,intensity;
    int tres,t1,t2,t3;
    ponto t;
    vertice v;
    char delim[] = " ";
    char line[128];
	char str[10] = ".ply";
	strcat(arquivo, str);
	arq = fopen(arquivo, "r"); //abre o arquivo txt
	printf("Lendo arquivo");
	//enquanto não for fim de arquivo o looping será executado
	 //e será impresso o texto
    if(arq != NULL){
        while(fgets(line, sizeof line, arq) != NULL){
            printf("%s  contador %d",line,cont);
            cont++;
            if(cont >= 12)
                flag = 1;
            if(flag == 1){
               // printf("%s flag: %d",line,flag);

                while ((fscanf(arq, "%f %f %f %f %f\n", &cx, &cy, &cz, &confidence, &intensity)) != EOF && cx != 3) {
                    v.x = cx;
                    v.y = cy;
                    v.z = cz;
                   // printf("vertices: %f %f %f \n", cx, cy, cz);

                   vertices.push_back(v);
                }
                 //   printf("v.x %f %f %f ", vertices.at(0).x, vertices.at(0).y, vertices.at(0).z);
               // printf("TT triangulos: %d %d %d", (int)cx, (int)cy, (int)cz);
               if(cx==3)
               {
                fscanf(arq, "%d %d %d",&t1, &t2,&t3);
                t.x = t1;
                t.y = t2;
                t.z = t3;
                }
                else {
               // printf("erro\n");
                }

               // printf("triangulos: %d %d %d %d\n", tres, t1, t2, t3);
               // printf(" t: %d", t3);

                while ((fscanf(arq, "%d %d %d %d\n", &tres, &t1, &t2, &t3)) != EOF) {
                    t.x = t1;
                    t.y = t2;
                    t.z = t3;
                    if(cont==0)
                    {
                       // printf("triangulos: %d %d %d %d\n", tres, t1, t2, t3);
                    }
                    if(tres!= 3 && cont2==0)
                    {         //   printf("triangulos: %d %d %d %d\n", tres, t1, t2, t3);
                                cont2++;}
                    triangulos.push_back(t);
                }
               // printf("triangulos inseridos.. ");
            }
        }
        fclose(arq);
        printf("Arquivo foi lido");
        constroiTriangulos();
    }

}
void showMenu()
{
    int op;
    system("cls");
    printf("\n=== MENU ===");
    printf("\ns - Salva o arquivo");
    printf("\nl - Carrega arquivo salvo na pasta");
    printf("\nF12 - Visualização em Modo Tela Cheia");
    printf("\n . - Aumentar a Espessura do Modelo");
    printf("\n , - Diminuir a Espessura do Modelo\n");
    printf("\n Setas direcionais para esquerda e Direita - Alternam grupos de desenho \n") ;
    printf("\n Setas direcionais para cima e baixo - Aumenta e Diminui altura do modelo \n") ;
    printf("\nEsc - Sair\n");

}
void display(void)
{
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(1,1,1,0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glViewport(0,0,width,height);     //VIEW PORT DO MODELO 3D

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    gluPerspective(60.0, (GLfloat) width/(GLfloat) height, 0.1, 200.0);


    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    gluLookAt (0.0, 0.0, zdist+zom, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
    glPushMatrix();
        glRotatef( rotationY, 0.0, 0.3, 0.0 );
        glRotatef( rotationX, 0.3, 0.0, 0.0 );

        sprintf(title, "Quantidade de triangulos: %d ", qtd_triangulos);//Muda titulo da tela
        glutSetWindowTitle(title);
           /* for(int i=0; i<vertices.size(); i++)
            {
                glColor3f(0, 1,0);
                glPointSize(2);
                glBegin(GL_POINTS);
                    glVertex3f(vertices.at(i).x, vertices.at(i).y, vertices.at(i).z);
                glEnd();
            }*/

            if(grupo >= 1){

                if(grupo == 1)
                zom = 0.1;
                else if(grupo == 2)
                zom = 0.1;
                else if(grupo == 3)
                zom = 5.0;
                else if(grupo == 4)
                zom = 0.1;
                else if(grupo == 5)
                zom = 0.2;
                else if(grupo == 6)
                zom = 9.0;
                desenhaPLY();
            }
    glPopMatrix();
    glutSwapBuffers();
}

void idle ()
{
    glutPostRedisplay();
}

void reshape (int w, int h)
{
    width = w;
    height = h;

    glViewport (0, 0, (GLsizei) w, (GLsizei) h);
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    gluPerspective(60.0, (GLfloat) w/(GLfloat) h, 0.01, 200.0);
}

// Special Keys callback
void specialKeysPress(int key, int x, int y)
{
    switch(key)
    {
        case GLUT_KEY_RIGHT:

                grupo += 1;  // Passa para o grupo anterior na lista de seleção pelo teclado
                if(grupo == 1){
                    vertices.clear();
                    triangulos.clear();
                    triangulosPLY.clear();
                    char nome[] = "bunny";
                    leArquivoPLYBunny(nome);
                }else if(grupo == 2){
                    vertices.clear();
                    triangulos.clear();
                    triangulosPLY.clear();
                    char nome[] = "budda";
                    leArquivoPLY(nome);
                } else if(grupo == 3){
                    vertices.clear();
                    triangulos.clear();
                    triangulosPLY.clear();
                    char nome[] = "cow";
                    leArquivoPLYCow(nome);
                } else if(grupo == 4){
                    vertices.clear();
                    triangulos.clear();
                    triangulosPLY.clear();
                    char nome[] = "dragon";
                    leArquivoPLY(nome);
                } else if(grupo == 5){
                    vertices.clear();
                    triangulos.clear();
                    triangulosPLY.clear();
                    char nome[] = "dragon_full";
                    leArquivoPLY(nome);
                } else if(grupo == 6){
                    vertices.clear();
                    triangulos.clear();
                    triangulosPLY.clear();
                    char nome[] = "snowman";
                    leArquivoPLYSnowman(nome);
                }
            break;
        case GLUT_KEY_LEFT:
                if(grupo > 0){
                grupo -= 1;  // Passa para o grupo anterior na lista de seleção pelo teclado
                if(grupo == 1){
                    vertices.clear();
                    triangulos.clear();
                    triangulosPLY.clear();
                    char nome[] = "bunny";
                    leArquivoPLYBunny(nome);
                }else if(grupo == 2){
                    vertices.clear();
                    triangulos.clear();
                    triangulosPLY.clear();
                    char nome[] = "budda";
                    leArquivoPLY(nome);
                } else if(grupo == 3){
                    vertices.clear();
                    triangulos.clear();
                    triangulosPLY.clear();
                    char nome[] = "cow";
                    leArquivoPLYCow(nome);
                } else if(grupo == 4){
                    vertices.clear();
                    triangulos.clear();
                    triangulosPLY.clear();
                    char nome[] = "dragon";
                    leArquivoPLY(nome);
                } else if(grupo == 5){
                    vertices.clear();
                    triangulos.clear();
                    triangulosPLY.clear();
                    char nome[] = "dragon_full";
                    leArquivoPLY(nome);
                } else if(grupo == 6){
                    vertices.clear();
                    triangulos.clear();
                    triangulosPLY.clear();
                    char nome[] = "snowman";
                    leArquivoPLYSnowman(nome);
                }
                }
            break;
        case GLUT_KEY_F12:
                if (fullscreen)
                {
                    glutReshapeWindow(800, 400);
                    fullscreen =0;
                } else {
                    glutFullScreen();
                    fullscreen =1;
                }
            break;
        default:
            printf("\nPressionou outra tecla especial não mapeada!");
            break;
    }
    glutPostRedisplay();
}

void keyboard(unsigned char key, int x, int y)
{

    char nome[20];
	switch (tolower(key))
	{
	case 27:
		exit(0);
		break;
    case 'm':
        printf("Digite o nome do arquivo: ");
		scanf("%s", &nome);
        leArquivoPLY(nome);
        break;
	}
}

// Motion callback
void motion(int x, int y)
{
    rotationX += (float)(y - last_y);
    rotationY += (float)(x - last_x);

	last_x = x;
	last_y = y;
}

// Mouse callback
void mouse(int button, int state, int x, int y)
{
    if(button == 3) // Scroll up
    {
        zdist+=0.5f;
    }
    if(button == 4) // Scroll Down
    {
        zdist-=0.5f;
    }
}

/// Main
int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    showMenu();
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize (800, 400);
    glutInitWindowPosition (300, 200);
    glutCreateWindow (argv[0]);
    sprintf(title, "Trabalho D2");
    glutSetWindowTitle(title);
    init ();
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutMouseFunc( mouse );
    glutMotionFunc( motion );
    glutSpecialFunc( specialKeysPress );
    glutKeyboardFunc(keyboard);
    glutIdleFunc(idle);
    glutMainLoop();
    return 0;
}
